#include <iostream>
#include <iomanip>
using namespace std;
int main() {
  float c, k, f, step, suhuakhir, suhuawal;
  cout << "masukan suhu awal = ";
  cin >> suhuawal;
  cout << "masukan step = ";
  cin >> step;
  cout << "masukan suhu akhir = ";
  cin >> suhuakhir;
  cout << "-----------------------------------";
  cout << endl;
  cout << "| celcius | fahrenheit |  kelvin  |";
  cout<< endl;
  cout << "-----------------------------------";
  cout << endl;
  for(c = suhuawal; c <= suhuakhir; c += step) {
    k = c+273.15;
    f = c*1.8+32;
    cout << "|  " << setw(5) << fixed << setprecision(2) << c
         << "  |  " << setw(8) << fixed << setprecision(2) << f
         << "  |  " << setw(2) << fixed << setprecision(2) << k << "  |" << endl;
    }
   cout << "-----------------------------------";
  }
