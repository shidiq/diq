#include <iostream>
#include <stdlib.h>
#include <iomanip>

using namespace std;

int main()
{
    system("clear");

    int JumlahAnak = 0;
    float GajiKotor = 0.0, Tunjangan = 0.0, PersenTunjangan = 0.0, Gajibersih = 0,0;

    PersenTunjangan = 0.2;
    cout << "Gaji Kotor ? "; cin >> GajiKotor;
    cout << "Jumlah Anak ? "; cin >> JumlahAnak;
    if (JumlahAnak > 2)
    {
        PersenTunjangan = 0.3;
    }
    
    if (JumlahAnak < 2)
    {
        Tunjangan = 0.0;
    }


    Tunjangan = PersenTunjangan * GajiKotor;
    Gajibersih = GajiKotor + Tunjangan;
    cout << "Besar Tunjangan = Rp " << setprecision(2) << Tunjangan << endl;
    cout << "Besar Gaji bersih = Rp " << setprecision(2) << Gajibersih << endl;

    return 0;
}
