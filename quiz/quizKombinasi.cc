#include <iostream>
using namespace std;
unsigned long long kombinasi(int n, int r) {
    if (r == 0 || r == n) {
        return 1;
    } else {
        return kombinasi(n - 1, r - 1) + kombinasi(n - 1, r);
    }
}

int main() {
    int tingkat;
    cout << "Masukkan N: ";
    cin >> tingkat;

    for (int i = 0; i < tingkat; i++) {
        for (int j = 0; j < tingkat - i - 1; j++) {
            cout << " ";
        }
        for (int j = 0; j <= i; j++) {
            cout << kombinasi(i, j) << " ";
        }
        cout << endl;
    }

    return 0;
}
