#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

int main()
{
    int N, i = 1;
    float Data, Total, Ratarata;
    cout << "Berapa banyak data : ";
    cin >> N;
    do
    {
        cout << "Masukkan Data ke " << i << " : ";
        i++;
        cin >> Data;
        Total += Data;

    }while (i <= N);
    cout << "Banyaknya data : " << N << endl;
    cout << "Total nilai data : " << Total << endl;
    Ratarata = Total/N;
    cout << "Rata-rata nilai data : " << Ratarata << endl;
}
