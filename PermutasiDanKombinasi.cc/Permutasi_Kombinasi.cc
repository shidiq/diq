#include <iostream>

using namespace std;


int faktorial(int n) {
    if (n == 0 || n == 1) {
    return 1;
    } else {
    return n * faktorial(n - 1);
    }
}


void hitungPermutasi() {
    int n, r;
    cout << "Masukkan nilai n: ";
    cin >> n;
    cout << "Masukkan nilai r: ";
    cin >> r;

    if (n >= r && n >= 0 && r >= 0) {
    int hasilPermutasi = faktorial(n) / faktorial(n - r);
    cout << "Permutasi dari " << n << " dan " << r << " adalah: " << hasilPermutasi << endl;
    } else {
    cout << "Masukan tidak valid untuk permutasi." << endl;
    }
}


void hitungKombinasi() {
    int n, r;
    cout << "Masukkan nilai n: ";
    cin >> n;
    cout << "Masukkan nilai r: ";
    cin >> r;

    if (n >= r && n >= 0 && r >= 0) {
    int hasilKombinasi = faktorial(n) / (faktorial(r) * faktorial(n - r));
    cout << "Kombinasi dari " << n << " dan " << r << " adalah: " << hasilKombinasi << endl;
    } else {
    cout << "Masukan tidak valid untuk kombinasi." << endl;
    }
}

int main() {
    int pilihan;

    do {
    cout << "Menu:\n";
    cout << "1. Hitung Permutasi\n";
    cout << "2. Hitung Kombinasi\n";
    cout << "0. Keluar\n";
    cout << "Pilih menu (0/1/2): ";
    cin >> pilihan;

    
    switch (pilihan) {
    case 1:
    hitungPermutasi();
    break;
    case 2:
    hitungKombinasi();
    break;
    case 0:
    cout << "Terima kasih. Program selesai.\n";
    break;
    default:
    cout << "Pilihan tidak valid. Silakan coba lagi.\n";
    break;
    }

    } while (pilihan != 0);

    return 0;
}
